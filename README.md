# Working Group Events

This working group is about event organization and has no defined end.

## Some ideas

Possible topics:
  - Room organization
  - Organize speakers / presentations
  - Casual events such as coffe-talks, regulars table
  - Other events?
  - Suitable times / weekdays / locations / durations (consider individual restrictions/preferences/accessability, maybe sync with "community survey" working group)

Maybe:
  - checkout events from german / UK / US / ... RSE groups for inspriation.
  - develop checklist for event organization or guidelines.

Inspirations:
  - https://scotrse.github.io/
  - https://cerse.github.io/CerseMeetingHandBook.html


